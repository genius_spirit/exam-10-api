const express = require("express");
const router = express.Router();

const createRouter = db => {
  router.get("/", (req, res) => {
    const param = req.query.news_id;

    if (param) {
      db.query("SELECT * FROM `comments` WHERE `news_id`= ?", [param], (error, results) => {
          if (error) throw error;
          res.send(results);
        }
      );
    } else {
      db.query("SELECT * FROM `comments`", (error, results) => {
        if (error) throw error;
        res.send(results);
      });
    }
  });

  router.post('/', (req, res) => {
    const data = req.body;

    if (data.author === '') {
      data.author = 'Anonymous';
    }

    if (data.newsId && data.comment) {
      const newsId = req.body.newsId;

      db.query("SELECT * FROM `news` WHERE `id`= ?", [newsId], (error, results) => {
        if (error) throw error;
        if (results.length !== 0){
          db.query('INSERT INTO `comments` (`news_id`, `author`, `comment`) VALUES (?, ?, ?)',
            [data.newsId, data.author, data.comment],
            (error, results) => {
              if (error) throw error;
              data.id = results.insertId;
              res.send(data);
            }
          )
        } else res.status(400).send(`Can not find news with id=${newsId}`);
      });
    } else res.status(400).send('Fields "news_id, comment" can not be blank');
  });

  router.delete("/:id", (req, res) => {
    const id = req.params.id;
    db.query("DELETE FROM `comments` WHERE `id`= ?", [id], (error, results) => {
      if (error) throw error;
      res.send("комментарий удален");
    });
  });

  return router;
};

module.exports = createRouter;
