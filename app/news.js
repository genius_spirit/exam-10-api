const express = require("express");
const multer = require("multer");
const path = require("path");
const nanoid = require("nanoid");
const config = require("../config");
const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({ storage });

const createRouter = db => {

  router.get("/", (req, res) => {
    db.query(
      "SELECT `id`, `title`, `image`, `date` FROM `news`",
      (error, results) => {
        if (error) throw error;
        res.send(results);
      }
    );
  });

  router.get("/:id", (req, res) => {
    const id = req.params.id;
    db.query("SELECT * FROM `news` WHERE `id`= ?", [id], (error, results) => {
      if (error) throw error;
      res.send(results);
    });
  });

  router.post("/", upload.single("image"), (req, res) => {
    const data = req.body;

    if (req.file) {
      data.image = req.file.filename;
    } else {
      data.image = null;
    }

    if (data.title && data.content) {
      db.query(
        "INSERT INTO `news` (`title`, `content`, `image`) VALUES (?, ?, ?)",
        [data.title, data.content, data.image],
        (error, results) => {
          if (error) throw error;
          data.id = results.insertId;
          data.date = new Date().toISOString();
          res.send(data);
        }
      );
    } else
      res
        .status(400)
        .send("Заголовок новости и содержимое не должны быть пустыми полями");
  });

  router.delete("/:id", (req, res) => {
    const id = req.params.id;
    const data = req.body;
    db.query("DELETE FROM `news` WHERE `id`= ?", [id], (error, results) => {
      if (error) throw error;
      data.id = id;
      res.send(data);
    });
  });

  return router;
};

module.exports = createRouter;
